Features:
1.) Customer registration
2.) Worker registration
3.) Customer can create task
4.) Worker can accept and complete task
5.) Worker can filter task based on status
6.) Worker can filter task based on category
7.) Worker can rate customer after task completion
8.) Customer can rate worker after task completion
9.) Worker can view its rating based on previous tasks
10.) Customer can view its rating based on previous tasks


Above is basic app for customer and worker marketplace.
Improvements:-
1.) We can add location with task.
2.) Customer can mark task location just like cabs app today.
3.) Customer could sort workers based on ratings.
4.) Worker Rating system can be made better by categorizing rating:
	a.) Time taken by worker for completion.
	b.) Quality of work done.
	c.) Communication skills of worker, etc
5.) Similarly, above can be done for customer also:
	a.) How well the customer treats its worker, etc.
6.) Spoken languages can be added by worker.
7.) Worker can sort tasks based on ratings of customer, distance from task, etc.
8.) Phone calls can be made configurable(allowed/disallowed) by workers/customers.
9.) Chatting feature can be added.
10.) Notify workers for long pending tasks.
11.) Better rated workers can be suggested to increase charges based on the market.
12.) Customer can sort workers based on cost.
13.) Charges can be varied with day. Eg workers can charge 10% extra for sundays or holidays.
14.) Workers can mark them active/inactive.

Improvements in tech:-
We can change the whole stack.
I used django because I have worked on it before and there is time limit for the above task.
It is better to complete task then to try for the best and not able to complete the assignment.

