from django.contrib.auth import login, authenticate
from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.core import serializers
from horauc.forms import SignUpForm
from horauc.models import workerProfile, customerProfile, Task
from horauc.utils import user_is_customer, user_is_worker


def workerSignup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            import ipdb
            ipdb.set_trace()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            worker = workerProfile.objects.create(
                user = user
            )
            return redirect('home')
    else:
        form = SignUpForm()
    return render(request, 'signup.html', {'form': form})


def customerSignup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            customer = customerProfile.objects.create(
                user = user
            )
            return redirect('home')
    else:
        form = SignUpForm()
    return render(request, 'signup.html', {'form': form})


def createTask(request):
    if request.method == 'POST':
        worker = workerProfile.objects.get(id=request.POST.worker_id)
        Task.objects.create(
            customer = request.user,
            worker = worker,
            category = request.POST.category,
        )


def updateTask(request):
    if request.method == 'POST':
        task = Task.objects.get(id=request.POST.task_id)
        task.status = request.POST.status
        task.save()


def rateCustomer(request):
    if request.method == 'POST':
        task = Task.objects.get(id=request.POST.task_id)
        if task.status == Task.COMPLETED:
            task.customer_rating = request.POST.rating
            task.save()


def rateWorker(request):
    if request.method == 'POST':
        task = Task.objects.get(id=request.POST.task_id)
        if task.status == Task.COMPLETED:
            task.worker_rating = request.POST.rating
            task.save()


def filterTasks(request):
    if request.GET.status:
        if request.GET.category:
            tasks = Task.objects.filter(
                worker=request.user, 
                status=request.GET.status, 
                category=request.GET.category
            )
        else:
            tasks = Task.objects.filter(
                worker=request.user, 
                status=request.GET.status
            )
    else:
        if request.GET.category:
            tasks = Task.objects.filter(
                worker=request.user,
                category=request.GET.category
            )
        else:
            tasks = Task.objects.filter(
                worker=request.user
            )

    response = serializers.serialize('json', tasks)
    return HttpResponse(response)


def rating(request):
    if user_is_worker(request.user):
        tasks = Task.objects.filter(worker=request.user, status=Task.COMPLETED)
    elif user_is_customer(request.user):
        tasks = Task.objects.filter(customer=request.user, status=Task.COMPLETED)

    count = 0
    totalRating = 0
    for task in tasks:
        totalRating = rating + task.workerProfile
        count = count + 1
    return HttpResponse(totalRating/count)



def home(request):
    return HttpResponse('hi')