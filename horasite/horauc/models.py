from django.db import models
from django.contrib.auth.models import User


class workerProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    location = models.CharField(max_length=30, blank=True)
    birth_date = models.DateField(null=True, blank=True)


class customerProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    location = models.CharField(max_length=30, blank=True)
    birth_date = models.DateField(null=True, blank=True)


class Task(models.Model):
	PENDING = 'pending'
	ACCEPTED = 'accepted'
	COMPLETED = 'completed'

	auto_increment_id = models.AutoField(primary_key=True)
	worker = models.OneToOneField(workerProfile, on_delete=models.CASCADE)
	customer = models.OneToOneField(customerProfile, on_delete=models.CASCADE)
	category = models.CharField(max_length=30, default='Others')
	status = models.CharField(max_length=30, default='pending')
	worker_rating = models.IntegerField()
	customer_rating = models.IntegerField()
	created_on = models.DateTimeField(auto_now_add=True)
	updated_on = models.DateTimeField(auto_now=True)