# Generated by Django 2.1 on 2018-08-14 11:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('horauc', '0002_task'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='task',
            name='id',
        ),
        migrations.AddField(
            model_name='task',
            name='auto_increment_id',
            field=models.AutoField(default=1, primary_key=True, serialize=False),
            preserve_default=False,
        ),
    ]
