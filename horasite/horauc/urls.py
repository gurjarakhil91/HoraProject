from django.urls import path

from . import views

urlpatterns = [
    path('workersignup', views.workerSignup, name='worker_signup'),
    path('customersignup', views.customerSignup, name='customer_signup'),
    path('home', views.home, name='home')
]