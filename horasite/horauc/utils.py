from horauc.models import customerProfile, workerProfile

def user_is_customer(user):
	customer = customerProfile.objects.filter(user=user)
	if customer:
		return True
	else:
		return False


def user_is_worker(user):
	worker = workerProfile.objects.filter(user=user)
	if worker:
		return True
	else:
		return False